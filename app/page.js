import Image from 'next/image'
import styles from './page.module.css'

export default function Home() {

  return (
    <main className={styles.main}>
      <h1>WELCOME TO MY WEBSITE!</h1>
      <h1>CHANGE CICD FIREBASE</h1>
      <h1>{process.env.NODE_ENV}</h1>

      <img src='/assets/vercel.svg' width={100} height={100} alt='VERCEL'/>
      <img src='/assets/next.svg' width={100} height={100} alt='NEXT /Assets'/>

      <br />
      <img src='/next.svg' width={100} height={100} alt='NEXT'/>
    </main>
  )
}
