/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'export',
    images: {
        unoptimized: true
    },
    reactStrictMode: true,
    trailingSlash: true,
    // assetPrefix: process.env.NODE_ENV === 'production' ? '/deploy-gitlab' : '',
}

module.exports = nextConfig
